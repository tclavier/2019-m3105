package fr.univlille.iutinfo.m3105.tp2.observer.controller;

import fr.univlille.iutinfo.m3105.tp2.domain.Temperature;
import fr.univlille.iutinfo.m3105.tp2.observer.model.ThermometerModel;
import org.junit.Assert;
import org.junit.Test;

import java.text.NumberFormat;

import static fr.univlille.iutinfo.m3105.tp2.domain.Unit.Celsius;
import static fr.univlille.iutinfo.m3105.tp2.domain.Unit.Fahrenheit;

public class ThermometerControllerTest {

    public static final double DELTA = 0.00001;

    @Test
    public void default_temperature_should_be_zero() {
        ThermometerModel model = new ThermometerModel();
        ThermometerController controller = new ThermometerController(model, Celsius);
        Assert.assertEquals(0, model.getTemperature(Celsius), DELTA);
    }

    @Test
    public void should_set_value_on_model_in_celsius() {
        ThermometerModel model = new ThermometerModel();
        ThermometerController controller = new ThermometerController(model, Celsius);
        controller.setValue(42);
        Assert.assertEquals(42, model.getTemperature(Celsius), DELTA);
    }

    @Test
    public void should_set_value_on_model_in_fahrenheit() {
        ThermometerModel model = new ThermometerModel();
        ThermometerController controller = new ThermometerController(model, Fahrenheit);
        controller.setValue(107.6);
        Assert.assertEquals(42, model.getTemperature(Celsius), DELTA);
    }

    @Test
    public void should_register_to_model() {
        ThermometerModel model = new ThermometerModel();
        ThermometerController controller = new ThermometerController(model, Celsius);
        MockObserver observer = new MockObserver();
        controller.register(observer);
        controller.setValue(42);
        Assert.assertEquals(new Temperature(42, Celsius), observer.getTemperature());
    }

    @Test
    public void should_set_value_on_model_in_string() {
        ThermometerModel model = new ThermometerModel();
        ThermometerController controller = new ThermometerController(model, Celsius);
        controller.setValue("42");
        Assert.assertEquals(42, model.getTemperature(Celsius), DELTA);
    }

}