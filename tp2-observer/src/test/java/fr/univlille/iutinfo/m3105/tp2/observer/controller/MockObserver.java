package fr.univlille.iutinfo.m3105.tp2.observer.controller;

import fr.univlille.iutinfo.m3105.tp2.domain.Temperature;

import java.util.Observable;
import java.util.Observer;

public class MockObserver implements Observer {
    private Temperature temperature;

    public Temperature getTemperature() {
        return temperature;
    }

    @Override
    public void update(Observable observable, Object o) {
        temperature = (Temperature) o;
    }
}
