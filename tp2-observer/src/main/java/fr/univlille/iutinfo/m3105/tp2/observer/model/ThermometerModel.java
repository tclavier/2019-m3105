package fr.univlille.iutinfo.m3105.tp2.observer.model;

import fr.univlille.iutinfo.m3105.tp2.domain.Temperature;
import fr.univlille.iutinfo.m3105.tp2.domain.Unit;

import java.util.Observable;

public class ThermometerModel extends Observable {
    Temperature temperature = new Temperature(0, Unit.Celsius);

    public double getTemperature(Unit unit) {
        return temperature.getValue(unit);
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
        this.setChanged();
        this.notifyObservers(temperature);
    }
}
