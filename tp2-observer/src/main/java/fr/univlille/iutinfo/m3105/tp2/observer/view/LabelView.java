package fr.univlille.iutinfo.m3105.tp2.observer.view;

import fr.univlille.iutinfo.m3105.tp2.domain.Temperature;
import fr.univlille.iutinfo.m3105.tp2.domain.Unit;
import fr.univlille.iutinfo.m3105.tp2.observer.controller.ThermometerController;
import javafx.beans.property.StringProperty;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Observable;
import java.util.Observer;

public class LabelView implements Observer {
    private ThermometerController controller;
    private Unit unit;
    private TextField label;

    public LabelView(ThermometerController controller, Unit unit) {
        this.controller = controller;
        this.unit = controller.getUnit();
        controller.register(this);
    }

    public void start() {
        Stage stage = new Stage();
        label = new TextField();
        label.setPrefWidth(300);
        label.setText(controller.getValue(unit));
        label.setOnAction(e -> {
            controller.setValue(label.getText());
        });
        Scene scene = new Scene(label);
        stage.setTitle("Température en °" + unit.getSymbol());
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void update(Observable observable, Object o) {
        Temperature temperature = (Temperature) o;
        label.setText(String.valueOf(temperature.getValue(unit)));
    }
}
