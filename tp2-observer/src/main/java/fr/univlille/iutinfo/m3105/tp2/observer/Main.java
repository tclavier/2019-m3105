package fr.univlille.iutinfo.m3105.tp2.observer;

import fr.univlille.iutinfo.m3105.tp2.domain.Unit;
import fr.univlille.iutinfo.m3105.tp2.observer.controller.ThermometerController;
import fr.univlille.iutinfo.m3105.tp2.observer.model.ThermometerModel;
import fr.univlille.iutinfo.m3105.tp2.observer.view.LabelView;
import fr.univlille.iutinfo.m3105.tp2.observer.view.ThermometerView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage stage) {
        ThermometerModel model = new ThermometerModel();
        ThermometerController celsiusController = new ThermometerController(model, Unit.Celsius);
        ThermometerView celsiusThermometerView = new ThermometerView(celsiusController);
        ThermometerController fahrenheitController = new ThermometerController(model, Unit.Fahrenheit);
        ThermometerView fahrenhietThermometerView = new ThermometerView(fahrenheitController);
        LabelView celsiusView = new LabelView(celsiusController, Unit.Celsius);
        LabelView fahrenheitView = new LabelView(fahrenheitController, Unit.Fahrenheit);

        celsiusThermometerView.start(stage);
        fahrenhietThermometerView.start(new Stage());
        celsiusView.start();
        fahrenheitView.start();
    }
}
