package fr.univlille.iutinfo.m3105.tp2.observer.view;

import fr.univlille.iutinfo.m3105.tp2.domain.Temperature;
import fr.univlille.iutinfo.m3105.tp2.domain.Unit;
import fr.univlille.iutinfo.m3105.tp2.observer.controller.ThermometerController;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

import java.util.Observable;
import java.util.Observer;

import static fr.univlille.iutinfo.m3105.tp2.domain.Temperature.TEMP_MAX;
import static fr.univlille.iutinfo.m3105.tp2.domain.Temperature.TEMP_MIN;
import static javafx.geometry.Orientation.VERTICAL;

public class ThermometerView implements Observer {
    private ThermometerController controller;
    private Unit unit;
    private Slider slider;

    public ThermometerView(ThermometerController controller) {
        this.controller = controller;
        this.unit = controller.getUnit();
        controller.register(this);
    }

    public void start(Stage stage) {

        final double maxValue = TEMP_MAX.convertTo(unit).getValue();
        final double minValue = TEMP_MIN.convertTo(unit).getValue();
        slider = new Slider(minValue, maxValue, controller.getValue());
        slider.setOrientation(VERTICAL);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setValueChanging(true);
        slider.setMajorTickUnit(10);
        slider.setMinorTickCount(5);
        slider.setOnKeyPressed(e -> {
            controller.setValue(slider.getValue());
        });
        slider.setOnMouseMoved(e -> {
            controller.setValue(slider.getValue());
        });
        Scene scene = new Scene(slider);
        stage.setTitle("T en °" + unit.getSymbol());
        stage.setWidth(150);
        stage.setHeight(400);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void update(Observable observable, Object o) {
        Temperature temperature = (Temperature) o;
        slider.setValue(temperature.getValue(unit));
    }
}
