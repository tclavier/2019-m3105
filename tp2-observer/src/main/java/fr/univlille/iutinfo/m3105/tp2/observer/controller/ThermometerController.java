package fr.univlille.iutinfo.m3105.tp2.observer.controller;

import fr.univlille.iutinfo.m3105.tp2.domain.Temperature;
import fr.univlille.iutinfo.m3105.tp2.domain.Unit;
import fr.univlille.iutinfo.m3105.tp2.observer.model.ThermometerModel;

import java.text.NumberFormat;
import java.util.Observer;

public class ThermometerController {

    private ThermometerModel model;
    private Unit unit;

    public ThermometerController(ThermometerModel model, Unit unit) {
        this.model = model;
        this.unit = unit;
    }

    public double getValue() {
        return model.getTemperature(unit);
    }

    public void setValue(double value) {
        model.setTemperature(new Temperature(value, unit));
    }

    public Unit getUnit() {
        return unit;
    }

    public void register(Observer observer) {
        model.addObserver(observer);
    }

    public String getValue(Unit unit) {
        return NumberFormat.getInstance().format(model.getTemperature(unit));
    }

    public void setValue(String text) {
        final Temperature temperature = new Temperature(Double.parseDouble(text), unit);
        model.setTemperature(temperature);
    }
}
