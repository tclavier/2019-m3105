package fr.univlille.iutinfo.m3105.tp2.property.view;

import fr.univlille.iutinfo.m3105.tp2.domain.Unit;
import javafx.beans.property.StringProperty;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LabelView {
    private StringProperty value;
    private Unit unit;

    public LabelView(StringProperty stringProperty, Unit unit) {
        this.value = stringProperty;
        this.unit = unit;
    }

    public void start() {
        Stage stage = new Stage();
        TextField label = new TextField();
        label.setPrefWidth(300);
        label.textProperty().bindBidirectional(value);
        Scene scene = new Scene(label);
        stage.setTitle("Température en °" + unit.getSymbol());
        stage.setScene(scene);
        stage.show();
    }
}
