package fr.univlille.iutinfo.m3105.tp2.property;

import fr.univlille.iutinfo.m3105.tp2.property.model.TemperatureModel;
import fr.univlille.iutinfo.m3105.tp2.property.view.LabelView;
import fr.univlille.iutinfo.m3105.tp2.property.view.ThermometerView;
import javafx.application.Application;
import javafx.stage.Stage;

import static fr.univlille.iutinfo.m3105.tp2.domain.Unit.Celsius;
import static fr.univlille.iutinfo.m3105.tp2.domain.Unit.Fahrenheit;

public class Main extends Application {
    TemperatureModel temperatureModel = new TemperatureModel();

    @Override
    public void start(Stage stage) {
        ThermometerView celsiusThermometerView = new ThermometerView(temperatureModel.getValueInCelsius(), Celsius);
        celsiusThermometerView.start(stage);
        ThermometerView fahrenhietThermometerView = new ThermometerView(temperatureModel.getValueInFahrenheit(), Fahrenheit);
        fahrenhietThermometerView.start(new Stage());
        LabelView celsiusView = new LabelView(temperatureModel.getLabelInCelsius(), Celsius);
        celsiusView.start();
        LabelView fahrenheitView = new LabelView(temperatureModel.getLabelInFahrenheit(), Fahrenheit);
        fahrenheitView.start();
    }
}
