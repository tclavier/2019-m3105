package fr.univlille.iutinfo.m3105.tp2.property.view;

import fr.univlille.iutinfo.m3105.tp2.domain.Unit;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

import static fr.univlille.iutinfo.m3105.tp2.domain.Temperature.TEMP_MAX;
import static fr.univlille.iutinfo.m3105.tp2.domain.Temperature.TEMP_MIN;
import static javafx.geometry.Orientation.VERTICAL;

public class ThermometerView {
    private DoubleProperty celsiusProperty;
    private Unit unit;

    public ThermometerView(DoubleProperty celsiusProperty, Unit unit) {
        this.celsiusProperty = celsiusProperty;
        this.unit = unit;
    }

    public void start(Stage stage) {
        final double maxValue = TEMP_MAX.convertTo(unit).getValue();
        final double minValue = TEMP_MIN.convertTo(unit).getValue();
        Slider slider = new Slider(minValue, maxValue, celsiusProperty.getValue());
        slider.setOrientation(VERTICAL);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setValueChanging(true);
        slider.setMajorTickUnit(10);
        slider.setMinorTickCount(5);
        slider.valueProperty().bindBidirectional(celsiusProperty);
        Scene scene = new Scene(slider);
        stage.setTitle("T en °" + unit.getSymbol());
        stage.setWidth(150);
        stage.setHeight(400);
        stage.setScene(scene);
        stage.show();
    }
}
