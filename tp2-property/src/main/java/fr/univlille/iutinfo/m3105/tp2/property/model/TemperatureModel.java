package fr.univlille.iutinfo.m3105.tp2.property.model;

import fr.univlille.iutinfo.m3105.tp2.domain.Temperature;
import fr.univlille.iutinfo.m3105.tp2.domain.Unit;
import fr.univlille.iutinfo.m3105.tp2.property.utils.BidirectionalBinding;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;

public class TemperatureModel {
    private DoubleProperty celsiusAsNumber = new SimpleDoubleProperty();
    private StringProperty celsiusAsString = new SimpleStringProperty();
    private StringConverter<Number> celsiusConverter = new NumberStringConverter();

    private DoubleProperty fahrenheitAsNumber = new SimpleDoubleProperty();
    private StringProperty fahrenheitAsString = new SimpleStringProperty();
    private StringConverter<Number> fahrenheitConverter = new NumberStringConverter();

    private BidirectionalBinding<Number, Number> temperatureConverter;

    public TemperatureModel() {
        Bindings.bindBidirectional(celsiusAsString, celsiusAsNumber, celsiusConverter);
        Bindings.bindBidirectional(fahrenheitAsString, fahrenheitAsNumber, fahrenheitConverter);
        temperatureConverter = new BidirectionalBinding<Number, Number>(celsiusAsNumber, fahrenheitAsNumber) {
            @Override
            protected Number convert(Number value) {
                Temperature from = new Temperature(value.doubleValue(), Unit.Celsius);
                return from.convertTo(Unit.Fahrenheit).getValue();
            }

            @Override
            protected Number inverseConvert(Number value) {
                Temperature from = new Temperature(value.doubleValue(), Unit.Fahrenheit);
                return from.convertTo(Unit.Celsius).getValue();
            }
        };
    }

    public void setTemperature(Temperature temperature) {
        celsiusAsNumber.setValue(temperature.convertTo(Unit.Celsius).getValue());
    }

    public DoubleProperty getValueInCelsius() {
        return celsiusAsNumber;
    }

    public StringProperty getLabelInCelsius() {
        return celsiusAsString;
    }

    public DoubleProperty getValueInFahrenheit() {
        return fahrenheitAsNumber;
    }

    public StringProperty getLabelInFahrenheit() {
        return fahrenheitAsString;
    }
}
