package fr.univlille.iutinfo.m3105.tp2.property;

import fr.univlille.iutinfo.m3105.tp2.domain.Temperature;
import fr.univlille.iutinfo.m3105.tp2.domain.Unit;
import fr.univlille.iutinfo.m3105.tp2.property.model.TemperatureModel;
import org.junit.Test;

import java.text.NumberFormat;

import static org.junit.Assert.assertEquals;

public class TemperatureModelTest {

    private static double DELTA = 0.000001;

    @Test
    public void should_read_celsius_value_as_double() {
        TemperatureModel model = new TemperatureModel();
        model.setTemperature(new Temperature(42, Unit.Celsius));
        assertEquals(42, model.getValueInCelsius().doubleValue(), DELTA);
    }

    @Test
    public void should_read_celsius_value_as_double_converted() {
        TemperatureModel model = new TemperatureModel();
        model.setTemperature(new Temperature(107.6, Unit.Fahrenheit));
        assertEquals(42, model.getValueInCelsius().getValue(), DELTA);
    }

    @Test
    public void should_read_celsius_value_as_string() {
        TemperatureModel model = new TemperatureModel();
        model.setTemperature(new Temperature(42, Unit.Celsius));
        assertEquals("42", model.getLabelInCelsius().getValue());
    }

    @Test
    public void should_update_celsius_value_and_read_label() {
        TemperatureModel model = new TemperatureModel();
        model.getValueInCelsius().setValue(42);
        assertEquals("42", model.getLabelInCelsius().getValue());
    }

    @Test
    public void should_update_fahrenheit_value_and_read_label() {
        TemperatureModel model = new TemperatureModel();
        model.getValueInFahrenheit().setValue(107.6);
        final String expected = NumberFormat.getInstance().format(107.6);
        assertEquals(expected, model.getLabelInFahrenheit().getValue());
    }

    @Test
    public void should_update_celsius_value_and_read_fahrenheit_label() {
        TemperatureModel model = new TemperatureModel();
        model.getValueInCelsius().setValue(42);
        final String expected = NumberFormat.getInstance().format(107.6);
        assertEquals(expected, model.getLabelInFahrenheit().getValue());
    }

    @Test
    public void should_update_fahrenheit_value_and_read_celsius_label() {
        TemperatureModel model = new TemperatureModel();
        model.getValueInFahrenheit().setValue(107.6);
        assertEquals("42", model.getLabelInCelsius().getValue());
    }
}
