package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import static fr.univlille.iutinfo.m3105.tp5.chifoumi.RoundResult.*;

public class Rock implements Shape {
    private static final Rock instance = new Rock();

    private Rock() {
    }

    public RoundResult versus(Rock other) {
        return Equality;
    }

    public RoundResult versus(Paper other) {
        return PaperWin;
    }

    public RoundResult versus(Scissors other) {
        return RockWin;
    }

    @Override
    public RoundResult versus(Shape other) {
        return other.versus(this);
    }

    public String toString() {
        return "rock";
    }

    public static Rock getInstance() {
        return instance;
    }
}
