package fr.univlille.iutinfo.m3105.tp5.chifoumi;

public interface Shape {

    RoundResult versus(Rock other);

    RoundResult versus(Paper other);

    RoundResult versus(Scissors other);

    RoundResult versus(Shape other);
}
