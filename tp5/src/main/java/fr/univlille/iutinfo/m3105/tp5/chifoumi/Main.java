package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import java.util.Scanner;

public class Main {

    public static final String QUERY = "\nWhat do you play ? Rock (r), Scissors (s), Paper (p), Quit (q): ";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(QUERY);
        String entry = scanner.next();
        while (!"q".equals(entry)) {
            final Shape player = Chifoumi.playFromString(entry);
            final Shape computer = Chifoumi.playRandom();
            System.out.println(player + " vs " + computer + ": " + player.versus(computer));
            System.out.println(QUERY);
            entry = scanner.next();
        }
    }
}
