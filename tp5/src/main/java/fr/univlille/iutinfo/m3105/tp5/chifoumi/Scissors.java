package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import static fr.univlille.iutinfo.m3105.tp5.chifoumi.RoundResult.*;

public class Scissors implements Shape {
    private static final Scissors instance = new Scissors();

    private Scissors() {
    }

    public static Scissors getInstance() {
        return instance;
    }

    @Override
    public RoundResult versus(Rock other) {
        return RockWin;
    }

    @Override
    public RoundResult versus(Paper other) {
        return ScissorsWin;
    }

    @Override
    public RoundResult versus(Scissors other) {
        return Equality;
    }

    @Override
    public RoundResult versus(Shape other) {
        return other.versus(this);
    }

    public String toString() {
        return "scissors";
    }
}
