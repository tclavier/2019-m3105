package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import java.util.*;

public class Chifoumi {
    private static Map<String, Shape> shapes = new HashMap<>();

    static {
        shapes.put("r", playRock());
        shapes.put("p", playPaper());
        shapes.put("s", playScissors());
    }

    public static Shape playFromString(String s) {
        return shapes.get(s);
    }

    public static Shape playRandom() {
        final Shape[] hands = shapes.values().toArray(new Shape[0]);
        int rnd = new Random().nextInt(hands.length);
        return hands[rnd];
    }

    public static Collection<Shape> allOptions() {
        return shapes.values();
    }

    public static Scissors playScissors() {
        return Scissors.getInstance();
    }

    public static Paper playPaper() {
        return Paper.getInstance();
    }

    public static Rock playRock() {
        return Rock.getInstance();
    }

}
