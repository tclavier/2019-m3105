package fr.univlille.iutinfo.m3105.tp5.chifoumi;

public enum RoundResult {
    Equality(), RockWin(), ScissorsWin(), PaperWin();
}
