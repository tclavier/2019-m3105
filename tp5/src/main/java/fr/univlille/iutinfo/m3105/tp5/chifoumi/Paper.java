package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import static fr.univlille.iutinfo.m3105.tp5.chifoumi.RoundResult.*;

public class Paper implements Shape {
    private static final Paper instance = new Paper();

    private Paper() {
    }

    public static Paper getInstance() {
        return instance;
    }

    public RoundResult versus(Rock other) {
        return PaperWin;
    }

    public RoundResult versus(Paper other) {
        return Equality;
    }

    public RoundResult versus(Scissors other) {
        return ScissorsWin;
    }

    @Override
    public RoundResult versus(Shape other) {
        return other.versus(this);
    }

    public String toString() {
        return "paper";
    }
}
