package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import org.junit.Test;

import static fr.univlille.iutinfo.m3105.tp5.chifoumi.Chifoumi.*;
import static fr.univlille.iutinfo.m3105.tp5.chifoumi.RoundResult.*;
import static org.junit.Assert.assertEquals;

public class RockTest {
    @Test
    public void rock_interact_rulls() {
        final Shape rock = playRock();
        assertEquals(Equality, playRock().versus(rock));
        assertEquals(PaperWin, playPaper().versus(rock));
        assertEquals(RockWin, playScissors().versus(rock));
    }
    @Test
    public void rock_string () {
        assertEquals("rock", playRock().toString());
    }
}