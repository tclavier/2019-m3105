package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class ChifoumiTest {
    @Test
    public void should_return_shape_from_first_letter() {
        assertEquals(Rock.class, Chifoumi.playFromString("r").getClass());
        assertEquals(Paper.class, Chifoumi.playFromString("p").getClass());
        assertEquals(Scissors.class, Chifoumi.playFromString("s").getClass());
    }

    @Test
    public void should_select_random_shape (){
        final Collection<Shape> allShapes = Chifoumi.allOptions();
        Shape hand = Chifoumi.playRandom();
        assertNotNull(hand);
        assertTrue(allShapes.contains(hand));
    }
    @Test
    public void should_have_3_possible_shapes (){
        final Collection<Shape> allShapes = Chifoumi.allOptions();
        assertEquals(3, allShapes.size());
    }

    @Test
    public void test_factory () {
        Chifoumi.playRock();
        Chifoumi.playScissors();
        Chifoumi.playPaper();
    }
}