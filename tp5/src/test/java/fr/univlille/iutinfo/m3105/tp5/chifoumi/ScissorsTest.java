package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import org.junit.Test;

import static fr.univlille.iutinfo.m3105.tp5.chifoumi.Chifoumi.*;
import static fr.univlille.iutinfo.m3105.tp5.chifoumi.RoundResult.*;
import static org.junit.Assert.assertEquals;

public class ScissorsTest {
    @Test
    public void scissors_interact_rulls() {
        final Shape scissors = playScissors();
        assertEquals(RockWin, playRock().versus(scissors));
        assertEquals(ScissorsWin, playPaper().versus(scissors));
        assertEquals(Equality, playScissors().versus(scissors));
    }

    @Test
    public void scissors_string() {
        assertEquals("scissors", playScissors().toString());
    }
}