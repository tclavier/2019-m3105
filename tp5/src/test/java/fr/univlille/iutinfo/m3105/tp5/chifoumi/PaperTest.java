package fr.univlille.iutinfo.m3105.tp5.chifoumi;

import org.junit.Test;

import static fr.univlille.iutinfo.m3105.tp5.chifoumi.Chifoumi.*;
import static fr.univlille.iutinfo.m3105.tp5.chifoumi.RoundResult.*;
import static org.junit.Assert.assertEquals;

public class PaperTest {
    @Test
    public void paper_interact_rulls() {
        final Shape paper = playPaper();
        assertEquals(PaperWin, playRock().versus(paper));
        assertEquals(Equality, playPaper().versus(paper));
        assertEquals(ScissorsWin, playScissors().versus(paper));
    }

    @Test
    public void paper_string() {
        assertEquals("paper", playPaper().toString());
    }
}