package fr.univlille.iutinfo.m3105.tp4.decorator;

public abstract class Topping extends Dessert{
    private final Dessert mainDessert;
    public Topping(Dessert dessert) {
        mainDessert = dessert;
    }


    String mainDessertAsString() {
        return mainDessert.toString();
     }

     public double price() {
        return 0.8 + mainDessert.price();
     }
}
