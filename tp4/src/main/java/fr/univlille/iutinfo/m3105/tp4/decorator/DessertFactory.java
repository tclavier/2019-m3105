package fr.univlille.iutinfo.m3105.tp4.decorator;

public class DessertFactory {
    public static Dessert doubleChocolatCake() {
        return new Chocolat(new Chocolat(new Cake()));
    }

    public static Dessert speciality() {
        return new Chocolat(new Almond(new Chocolat(new IceCream())));
    }
}
