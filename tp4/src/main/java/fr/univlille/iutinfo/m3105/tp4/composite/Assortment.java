package fr.univlille.iutinfo.m3105.tp4.composite;

import fr.univlille.iutinfo.m3105.tp4.decorator.Dessert;

import java.util.ArrayList;
import java.util.List;

public class Assortment implements Component {
    private final List<Dessert> desserts;

    public Assortment(Component... components) {
        List<Dessert> list = new ArrayList<>();
        for (Component e : components) {
            list.addAll(e.flatten());
        }
        this.desserts = list;
    }

    public double price() {
        return desserts.stream().mapToDouble(Dessert::price).sum() * 0.9;
    }

    @Override
    public List<Dessert> flatten() {
        return desserts;
    }
}
