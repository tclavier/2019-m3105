package fr.univlille.iutinfo.m3105.tp4.singleton;

import fr.univlille.iutinfo.m3105.tp4.decorator.*;

public class DessertMenu {

    private static final DessertMenu dessertMenu = new DessertMenu();

    private DessertMenu() {
    }

    public static DessertMenu getInstance() {
        return dessertMenu;
    }

    public Dessert speciality() {
        return DessertFactory.speciality();
    }
}
