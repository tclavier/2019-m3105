package fr.univlille.iutinfo.m3105.tp4.decorator;

import fr.univlille.iutinfo.m3105.tp4.decorator.Chocolat;
import fr.univlille.iutinfo.m3105.tp4.decorator.Dessert;
import fr.univlille.iutinfo.m3105.tp4.decorator.Topping;

public class Nuts extends Topping {
    public Nuts(Dessert dessert) {
        super(dessert);
    }

    public String toString() {
        return mainDessertAsString() + " - Nuts";
    }
}
