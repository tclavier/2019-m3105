package fr.univlille.iutinfo.m3105.tp4.decorator;

public class Cake extends Dessert {
    public String toString() {
        return "Cake";
    }

    @Override
    public double price() {
        return 17;
    }
}
