package fr.univlille.iutinfo.m3105.tp4.decorator;

public class Chocolat extends Topping {

    public Chocolat(Dessert dessert) {
        super(dessert);
    }

    public String toString() {
        return mainDessertAsString() + " - Chocolat";
    }
}
