package fr.univlille.iutinfo.m3105.tp4.composite;

import fr.univlille.iutinfo.m3105.tp4.decorator.Dessert;

import java.util.List;

public interface Component {
    public List<Dessert> flatten();
}
