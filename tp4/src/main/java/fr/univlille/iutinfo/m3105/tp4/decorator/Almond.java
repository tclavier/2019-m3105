package fr.univlille.iutinfo.m3105.tp4.decorator;

public class Almond extends Topping {
    public Almond(Dessert dessert) {
        super(dessert);
    }

    public String toString() {
        return mainDessertAsString() + " - Almond";
    }
}
