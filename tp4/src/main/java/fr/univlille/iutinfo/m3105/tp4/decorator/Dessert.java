package fr.univlille.iutinfo.m3105.tp4.decorator;

import fr.univlille.iutinfo.m3105.tp4.composite.Component;

import java.util.Arrays;
import java.util.List;

public abstract class Dessert implements Component {

    public abstract double price();
    @Override
    public List<Dessert> flatten() {
        return Arrays.asList(this);
    }
}
