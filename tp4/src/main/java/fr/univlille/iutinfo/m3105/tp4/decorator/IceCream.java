package fr.univlille.iutinfo.m3105.tp4.decorator;

public class IceCream extends Dessert{
    public String toString() {
        return "Ice cream";
    }

    @Override
    public double price() {
        return 9;
    }
}
