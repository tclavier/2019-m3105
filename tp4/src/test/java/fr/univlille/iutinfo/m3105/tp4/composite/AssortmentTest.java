package fr.univlille.iutinfo.m3105.tp4.composite;

import fr.univlille.iutinfo.m3105.tp4.decorator.Cake;
import org.junit.Test;

import static fr.univlille.iutinfo.m3105.tp4.decorator.DessertFactory.doubleChocolatCake;
import static fr.univlille.iutinfo.m3105.tp4.decorator.DessertFactory.speciality;
import static org.junit.Assert.assertEquals;

public class AssortmentTest {

    public static final double DELTA = 0.00001;

    @Test
    public void should_create_set_with_one_cake_and_one_icecream() {
        final Assortment assortment = new Assortment(doubleChocolatCake(), speciality());
    }

    @Test
    public void price_of_assortment_should_be_10pct_lower() {
        final Assortment assortment = new Assortment(doubleChocolatCake(), speciality());
        double expected = (18.6 + 11.4) * 0.9;
        assertEquals(expected, assortment.price(), DELTA);
    }

    @Test
    public void should_add_one_cake_and_one_assortment_in_assortment () {
        final Assortment firstSet = new Assortment(doubleChocolatCake(), speciality());
        final Assortment newSet = new Assortment(new Cake(), firstSet);
    }

    @Test
    public void reduction_of_assortment_should_be_compute_only_ones () {
        final Assortment firstSet = new Assortment(doubleChocolatCake(), speciality());
        final Assortment newSet = new Assortment(new Cake(), firstSet);
        assertEquals((18.6 + 11.4 + 17) * 0.9, newSet.price(), DELTA);
    }
}