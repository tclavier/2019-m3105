package fr.univlille.iutinfo.m3105.tp4.decorator;

import fr.univlille.iutinfo.m3105.tp4.singleton.DessertMenu;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DessertMenuTest {
    @Test
    public void should_create_speciality() {
        Dessert speciality = DessertMenu.getInstance().speciality();
        assertEquals("Ice cream - Chocolat - Almond - Chocolat", speciality.toString());
    }

    @Test
    public void factory_should_be_uniq (){
        assertEquals(DessertMenu.getInstance(), DessertMenu.getInstance());
    }

    /* It's not possible to do :
     * new DessertMenu();
     */
}