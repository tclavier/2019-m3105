package fr.univlille.iutinfo.m3105.tp4.decorator;

import org.junit.Test;

import static fr.univlille.iutinfo.m3105.tp4.composite.AssortmentTest.DELTA;
import static org.junit.Assert.assertEquals;

public class CakeTest {
    @Test
    public void should_create_chocolat_cake() {
        final Dessert chocolatCake = new Chocolat(new Cake());
        assertEquals("Cake - Chocolat", chocolatCake.toString());
    }

    @Test
    public void should_create_cake_with_chocolat_and_nuts() {
        final Dessert cake = new Nuts(new Chocolat(new Cake()));
        assertEquals("Cake - Chocolat - Nuts", cake.toString());
    }

    @Test
    public void should_create_ice_cream_with_almond_and_chocolat() {
        final Dessert iceCream = new Chocolat(new Almond(new IceCream()));
        assertEquals("Ice cream - Almond - Chocolat", iceCream.toString());
    }

    @Test
    public void should_get_price_of_cake(){
        final Dessert cake = new Cake();
        assertEquals(17, cake.price(), DELTA);
    }

    @Test
    public void should_get_price_of_cake_with_toppings(){
        final Dessert cake = new Chocolat(new Chocolat(new Cake()));
        assertEquals(18.6, cake.price(), DELTA);
    }
}