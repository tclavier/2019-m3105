# l’heure du choix

On s’intéresse ici à la modélisation d’un processus de sélection de représentants d’un groupe
d’individus (représentant des élèves, délégués syndicaux, élections municipales...). On modélise
d’abord la possibilité de procéder à un choix d’un individus parmi un ensemble d’individus donnés
avec l'interface `Preference`.

On donne la base de diagramme de classes suivante :

```mermaid
classDiagram

    class Preference
    Preference : choisir (List<Individu>) Individu
    
    class Individu
    Individu : - int id
    Individu : - String nom
    Individu : + Individu(Preferences, String, int)
    
    Individu o-- Preference : pref
    Preference <|-- Individu
    
```

Individu implémente l’interface `Preference` (méthode `choisir()`) et délègue la réalisation
du choix à son attribut `pref`.

## Question 1.1

Il nous faut maintenant une classe concrète qui implémente Preference. Pour cela, on va
associer aux Individus des points, celui qui en recueille le plus étant sélectionné. Notez que
l’interface Preference permet de choisir l’Individu qui a le plus de points, mais il faut aussi dans
PreferencePoint, une méthode pour affecter des points aux Individus. Il n’y a pas a priori
de candidats formels, tous les Individus sont des candidats potentiels.
Complétez ce diagramme en ajoutant une classe PreferencesPoint implémentant
Preference. Fournir des méthodes afin de pouvoir attribuer des points à un Individu et
accéder au nombre de points recueillis par un Individu (0 si il n’est pas répertorié). Donnez le
code de la classe PreferencesPoint.
(On n’implémentera pas, dans cette question, de mécanisme pour vérifier que chaque Individu
ne donne qu’un nombre finis et déterminé de points à son-sa-ses préféré-e-s)

# Des files de messages

Le principe des files de messages est le suivant : un ensemble de producteurs vont déposer des
messages dans une boite aux lettres. Un ensemble de consommateurs vont s'abonner à cette boite
aux lettres. À chaque réception de message (par la boite), les consommateurs vont prendre
connaissance du message.

Pour la suite nous allons utiliser le pattron de conception Observer / Observables pour implémenter cette file de messages.


## Question 2.1

À partir de la description précédente, produire le diagramme de classes faisant apparaître les
différents intervenants (producteurs, consommateurs, boites aux lettres, messages). Donnez un
ensemble minimal de méthodes et d’attributs pour que le mécanisme décrit fonctionne. On souhaite
l'utilisation du patron Observer/Observables mais vous ne pouvez pas utiliser
java.util.Observer ou java.util.Observable, et devez le réimplémenter à la main.

``` mermaid
classDiagram
    
    class Producer
    
    class LetterBox
    LetterBox : -List<Consumer> consumers
    LetterBox : -List<Message> messages
    LetterBox : +attach(Consumer)
    LetterBox : +detach(Consumer)
    LetterBox : +put(Message)
    LetterBox : +Message nextMessage()
    LetterBox : -notifyConsumers()
    
    class Consumer
    Consumer : +update()

    class Message    
```

## Question 2.2

Produire le diagramme de séquence montrant l’abonnement de deux consommateurs à la boite aux
lettres ; la réception d’un message par celle-ci provenant d’un unique producteur ; et la prise de
connaissance du message par les deux consommateurs.

``` mermaid
sequenceDiagram
    Consumer1 ->> LetterBox : attach(this)
    Consumer2 ->> LetterBox : attach(this)
    Producer  ->> LetterBox : put(Message)
    LetterBox ->> Consumer1 : update(this)
    LetterBox ->> Consumer2 : update(this)
    Consumer1 ->> LetterBox : nextMessage()
    Consumer2 ->> LetterBox : nextMessage()
    Consumer1 ->> LetterBox : detach(this)
    Consumer2 ->> LetterBox : detach(this)
```

## Question 4

Donner l’implémentation Java de la classe des boite aux lettres. Cette implémentation inclura : le
mécanisme d’abonnement à la boite aux lettres (vous ne pouvez pas utiliser java.util.Observer ou
Java.util.Observable,) ; la dépose d’un Message par un producteur ; la notification aux
consommateurs inscrits ; la possibilité pour les consommateurs de récupérer un message.

```java
class LetterBox {
    private final List<Consumer> consumers = new ArrayList<>();
    private final Stack<Message> messages = new Stack<>();

    public void attach(Consumer consumer) {
        consumers.add(consumer);
    }

    public void put(Message message) {
        messages.push(message);
        this.notifyConsumers();
    }

    private void notifyConsumers() {
        for (Consumer consumer: consumers) {
            consumer.update();
        }
    }

    public int count() {
        return this.messages.size();
    }

    public Message nextMessage() {
        return messages.pop();
    }
}

```
