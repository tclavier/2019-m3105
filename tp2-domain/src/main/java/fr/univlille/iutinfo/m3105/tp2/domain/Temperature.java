package fr.univlille.iutinfo.m3105.tp2.domain;

import static fr.univlille.iutinfo.m3105.tp2.domain.Unit.Celsius;

public class Temperature {
    public static final Temperature TEMP_MIN = new Temperature(-30, Celsius);
    public static final Temperature TEMP_MAX = new Temperature(200, Celsius);
    private double value;
    private Unit unit;

    public Temperature(double value, Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    public Temperature convertTo(Unit unit) {
        if ((this.unit == Unit.Fahrenheit) && (unit == Celsius)) {
            double newValue = (value - 32) * 5 / 9;
            return new Temperature(newValue, unit);
        }
        if (this.unit == Celsius && unit == Unit.Fahrenheit) {
            double newValue = value * 9 / 5 + 32;
            return new Temperature(newValue, unit);
        }
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (this.getClass() != obj.getClass())
            return false;

        Temperature other = (Temperature) obj;
        return value == other.value && unit == other.unit;
    }

    public double getValue() {
        return value;
    }

    public double getValue(Unit unit) {
        return this.convertTo(unit).getValue();
    }
}
