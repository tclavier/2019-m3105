package fr.univlille.iutinfo.m3105.tp2.domain;

public enum Unit {
    Celsius("C"), Fahrenheit("F");

    private final String symbol;

    Unit(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
