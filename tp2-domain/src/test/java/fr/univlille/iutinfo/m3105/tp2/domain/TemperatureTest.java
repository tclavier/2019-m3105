package fr.univlille.iutinfo.m3105.tp2.domain;

import org.junit.Test;

import static fr.univlille.iutinfo.m3105.tp2.domain.Unit.Fahrenheit;
import static fr.univlille.iutinfo.m3105.tp2.domain.Unit.Celsius;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TemperatureTest {
    @Test
    public void should_convert_from_fahrenheit_to_celsius() {
        Temperature fahrenheit = new Temperature(107.6, Fahrenheit);
        Temperature celsius = new Temperature(42, Celsius);
        assertEquals(celsius, fahrenheit.convertTo(Celsius));
    }

    @Test
    public void should_convert_from_celsius_to_fahrenheit() {
        Temperature celsius = new Temperature(42, Celsius);
        Temperature expected = new Temperature(107.6, Fahrenheit);
        Temperature actual = celsius.convertTo(Fahrenheit);
        assertEquals(expected, actual);
    }

    @Test
    public void should_not_convert_when_same_unit() {
        assertEquals(new Temperature(42, Celsius), new Temperature(42, Celsius).convertTo(Celsius));
        assertEquals(new Temperature(42, Fahrenheit), new Temperature(42, Fahrenheit).convertTo(Fahrenheit));
    }

    @Test
    public void should_be_equal_when_same_value_and_same_unit() {
        Temperature left = new Temperature(42, Celsius);
        Temperature right = new Temperature(42, Celsius);
        assertEquals(left, right);
    }

    @Test
    public void should_be_different_when_not_same_value_or_unit() {
        assertNotEquals(new Temperature(42, Fahrenheit), new Temperature(42, Celsius));
        assertNotEquals(new Temperature(107.6, Fahrenheit), new Temperature(42, Celsius));
        assertNotEquals(new Temperature(107.6, Celsius), new Temperature(42, Celsius));
    }

    @Test
    public void should_get_value_in_celsius_or_fahrenheit() {
        Temperature celsius = new Temperature(42, Celsius);
        double actual = celsius.getValue(Fahrenheit);
        assertEquals(107.6, actual, 0.00001);
    }
}
