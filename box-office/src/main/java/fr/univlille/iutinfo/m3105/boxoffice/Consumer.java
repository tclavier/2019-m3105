package fr.univlille.iutinfo.m3105.boxoffice;

public class Consumer {
    private final LettersBox lettersBox;

    public Consumer(LettersBox lettersBox) {
        this.lettersBox = lettersBox;
        lettersBox.attach(this);
    }

    public void update() {
        lettersBox.nextMessage();
    }
}
