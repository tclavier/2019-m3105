package fr.univlille.iutinfo.m3105.boxoffice;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class LettersBox {
    private final List<Consumer> consumers = new ArrayList<>();
    private final Stack<Message> messages = new Stack<>();

    public void attach(Consumer consumer) {
        consumers.add(consumer);
    }

    public void put(Message message) {
        messages.push(message);
        this.notifyConsumers();
    }

    private void notifyConsumers() {
        for (Consumer consumer: consumers) {
            consumer.update();
        }
    }

    public int count() {
        return this.messages.size();
    }

    public Message nextMessage() {
        return messages.pop();
    }
}
