package fr.univlille.iutinfo.m3105.boxoffice;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LettersBoxTest {
    @Test
    public void when_letter_is_received_consumers_should_be_notified() {
        LettersBox lettersBox = new LettersBox();
        MockConsumer consumer = new MockConsumer(lettersBox);
        lettersBox.put(new Message("Mon beau message"));
        Assert.assertTrue(consumer.isNotified());
    }
    @Test
    public void should_increase_number_of_letters() {
        LettersBox lettersBox = new LettersBox();
        assertEquals(0,lettersBox.count());
        lettersBox.put(new Message("Un message"));
        assertEquals(1, lettersBox.count());
    }
}