package fr.univlille.iutinfo.m3105.boxoffice;

public class MockConsumer extends Consumer{

    private boolean notified = false;

    public MockConsumer(LettersBox lettersBox) {
        super(lettersBox);
    }

    public boolean isNotified() {
        return notified;
    }

    @Override
    public void update() {
        notified = true;
    }
}
