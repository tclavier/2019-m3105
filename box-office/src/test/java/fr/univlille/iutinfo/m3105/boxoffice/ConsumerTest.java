package fr.univlille.iutinfo.m3105.boxoffice;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConsumerTest {
    @Test
    public void on_update_consumer_request_next_message() {
        final LettersBox lettersBox = new LettersBox();
        lettersBox.put(new Message("Mon beau message"));
        Consumer consumer = new Consumer(lettersBox);
        consumer.update();
        assertEquals(0, lettersBox.count());
    }
}