package fr.univlille.iutinfo.m3105.tp1.core;

import fr.univlille.iutinfo.m3105.tp1.domain.Message;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable {
    List<Observer> observers = new ArrayList<>();

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void detach(Observer observer) {
        observers.remove(observer);
    }

    public void notifyObservers(Message object) {
        for (Observer observer : observers) {
            observer.update(object);
        }
    }

    public void closeAll() {
        for (Observer observer: observers){
            observer.close();
        }
    }
}
