package fr.univlille.iutinfo.m3105.tp1.gui;

import fr.univlille.iutinfo.m3105.tp1.controller.ListenerController;
import fr.univlille.iutinfo.m3105.tp1.controller.RadioChatController;
import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class RadioChatWindow {
    private RadioChatController controller;
    private TextArea messages = new TextArea();
    private TextField inputMessage = new TextField();

    public RadioChatWindow(RadioChatController controller) {
        this.controller = controller;
    }

    public void start(Stage primaryStage) {
        VBox main = new VBox();
        Scene root = new Scene(main);
        primaryStage.setResizable(true);
        primaryStage.setTitle("Radio Chat");
        primaryStage.setOnCloseRequest(this::closeChilds);
        Button requestNewListener = new Button("Nouvel auditeur");
        requestNewListener.setOnAction(this::newListener);
        messages.setEditable(false);
        inputMessage.setOnAction(this::writeMessage);
        main.getChildren().addAll(requestNewListener, messages, inputMessage);
        primaryStage.setScene(root);
        primaryStage.show();
    }

    private void closeChilds(WindowEvent event) {
        controller.close();
    }

    private void writeMessage(ActionEvent event) {
        String line = inputMessage.getText();
        controller.send(line);
        inputMessage.clear();
        messages.setText(controller.getMessages());
    }

    private void newListener(ActionEvent actionEvent) {
        ListenerController listener = controller.newListner();
        new ListenerWindow(listener);
    }
}
