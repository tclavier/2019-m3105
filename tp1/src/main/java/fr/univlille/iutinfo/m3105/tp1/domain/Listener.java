package fr.univlille.iutinfo.m3105.tp1.domain;

import fr.univlille.iutinfo.m3105.tp1.core.GenericWindow;
import fr.univlille.iutinfo.m3105.tp1.core.Observer;

import java.util.ArrayList;
import java.util.List;

public class Listener implements Observer {
    private List<Message> messages = new ArrayList<>();
    public GenericWindow window;

    @Override
    public void update(Message message) {
        messages.add(message);
        if (window == null) {
            System.out.println("Warning, no window for this listener");
        } else {
            window.refresh();
        }
    }

    @Override
    public void close() {
        window.close();
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setUi(GenericWindow window) {
        this.window = window;
    }
}
