package fr.univlille.iutinfo.m3105.tp1.controller;

import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;
import fr.univlille.iutinfo.m3105.tp1.domain.RadioChat;

public class RadioChatController {
    RadioChat radioChat = new RadioChat();

    public String getMessages() {
        return Message.convertListToString(radioChat.getMessages());
    }

    public void send(String line) {
        radioChat.send(new Message(line));
    }

    public ListenerController newListner() {
        Listener listener = new Listener();
        radioChat.attach(listener);
        ListenerController listenerController = new ListenerController(listener);
        return listenerController;
    }

    public void close() {
        radioChat.closeAll();
    }
}
