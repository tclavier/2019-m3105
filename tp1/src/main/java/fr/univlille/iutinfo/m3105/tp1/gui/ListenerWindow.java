package fr.univlille.iutinfo.m3105.tp1.gui;

import fr.univlille.iutinfo.m3105.tp1.controller.ListenerController;
import fr.univlille.iutinfo.m3105.tp1.core.GenericWindow;
import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ListenerWindow implements GenericWindow {
    private ListenerController controller;
    private Stage stage;
    private TextArea messages;

    ListenerWindow(ListenerController controller) {
        this.controller = controller;
        this.controller.setUi(this);
        createStage().show();
    }

    private Stage createStage() {
        stage = new Stage();
        stage.setResizable(true);
        VBox main = new VBox();
        messages = createMessagesZone();
        main.getChildren().addAll(messages);
        Scene root = new Scene(main);
        stage.setScene(root);
        return stage;
    }

    private TextArea createMessagesZone() {
        TextArea messages = new TextArea();
        messages.setEditable(false);
        return messages;
    }

    @Override
    public void refresh() {
        messages.setText(controller.getMessages());
    }

    public void close() {
        stage.close();
    }
}
