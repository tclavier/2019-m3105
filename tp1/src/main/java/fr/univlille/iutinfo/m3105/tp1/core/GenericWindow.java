package fr.univlille.iutinfo.m3105.tp1.core;

import java.util.List;
import java.util.stream.Collectors;

public interface GenericWindow {
    public void refresh();

    void close();

}
