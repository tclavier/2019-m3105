package fr.univlille.iutinfo.m3105.tp1.core;

import fr.univlille.iutinfo.m3105.tp1.domain.Message;

public interface Observer {
    public void update(Message message);
    public void close();
}
