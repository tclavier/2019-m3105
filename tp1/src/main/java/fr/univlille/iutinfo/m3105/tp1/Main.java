package fr.univlille.iutinfo.m3105.tp1;

import fr.univlille.iutinfo.m3105.tp1.controller.RadioChatController;
import fr.univlille.iutinfo.m3105.tp1.gui.RadioChatWindow;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage) {
        RadioChatWindow radioChatWindow = new RadioChatWindow(new RadioChatController());
        radioChatWindow.start(stage);
    }
}
