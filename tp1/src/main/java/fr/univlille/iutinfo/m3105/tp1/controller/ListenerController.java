package fr.univlille.iutinfo.m3105.tp1.controller;

import fr.univlille.iutinfo.m3105.tp1.core.GenericWindow;
import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;

public class ListenerController {
    private Listener listener;

    public ListenerController(Listener listener) {
        this.listener = listener;
    }

    public String getMessages() {
        return Message.convertListToString(listener.getMessages());
    }

    public void setUi(GenericWindow ui){
        listener.setUi(ui);
    }
}
