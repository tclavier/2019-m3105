package fr.univlille.iutinfo.m3105.tp1.domain;

import fr.univlille.iutinfo.m3105.tp1.core.Observable;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;

import java.util.ArrayList;
import java.util.List;

public class RadioChat extends Observable {
    private List<Message> messages = new ArrayList<>();


    public void send(Message message) {
        messages.add(message);
        notifyObservers(message);
    }

    public List<Message> getMessages() {
        return messages;
    }
}
