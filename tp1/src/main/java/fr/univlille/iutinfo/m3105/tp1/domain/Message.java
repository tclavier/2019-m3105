package fr.univlille.iutinfo.m3105.tp1.domain;

import java.util.List;
import java.util.stream.Collectors;

public class Message {
    private String content = "";

    public Message() {
    }

    public Message(String content) {
        this.content = content;
    }

    public static String convertListToString(List<Message> messages) {
        return messages.stream().map(Message::getContent).collect(Collectors.joining("\n"));
    }

    public String getContent() {
        return content;
    }


}
