package fr.univlille.iutinfo.m3105.tp1.core;

import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;
import fr.univlille.iutinfo.m3105.tp1.domain.RadioChat;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class RadioChatTest {
    @Test
    public void test_when_write_message_listners_has_last_message() {
        Listener listener = new Listener();
        RadioChat radioTchat = new RadioChat();
        radioTchat.attach(listener);
        radioTchat.send(new Message("Message de test"));
        assertEquals("Message de test", getLastMessage(listener.getMessages()));
    }
    @Test
    public void test_when_write_multiple_messages_listners_should_return_last_message() {
        Listener listener = new Listener();
        RadioChat radioTchat = new RadioChat();
        radioTchat.attach(listener);
        radioTchat.send(new Message("Message de test 1"));
        radioTchat.send(new Message("Message de test 2"));
        assertEquals("Message de test 2", getLastMessage(listener.getMessages()));
    }


    @Test
    public void test_should_return_lastmessage(){
        RadioChat radioTchat = new RadioChat();
        radioTchat.send(new Message("Message de test"));
        assertEquals("Message de test", getLastMessage(radioTchat.getMessages()));
    }

    private String getLastMessage(List<Message> messages){
        return messages.get(messages.size() - 1).getContent();
    }
}