package fr.univlille.iutinfo.m3105.tp1.core;

import fr.univlille.iutinfo.m3105.tp1.domain.Message;

public class MockObserver implements Observer {
    private int notifyCount = 0;

    int getNotifyCount() {
        return notifyCount;
    }

    @Override
    public void update(Message message) {
        notifyCount++;
    }

    @Override
    public void close() {

    }
}
