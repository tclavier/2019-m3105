package fr.univlille.iutinfo.m3105.tp1.controller;

import fr.univlille.iutinfo.m3105.tp1.core.GenericWindow;
import fr.univlille.iutinfo.m3105.tp1.core.MockWindow;
import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class RadioChatControllerTest {
    @Test
    public void should_return_all_messages_as_string() {
        RadioChatController controller = new RadioChatController();
        controller.send("Nouveau message");
        assertEquals("Nouveau message", controller.getMessages());
    }

    @Test
    public void when_send_line_all_listners_should_receve_message() {
        RadioChatController controller = new RadioChatController();
        final ListenerController listener = controller.newListner();
        controller.send("Ligne de message");
        assertEquals("Ligne de message", listener.getMessages());
    }

    @Test
    public void new_listner_should_creat_new_instance() {
        RadioChatController controller = new RadioChatController();
        Assert.assertNotEquals(controller.newListner(), controller.newListner());
    }

    @Test
    public void closeAll_should_close_all_listnerWindow() {
        RadioChatController controller = new RadioChatController();
        ListenerController listener1 = controller.newListner();
        ListenerController listener2 = controller.newListner();
        MockWindow listenerWindow1 = new MockWindow();
        MockWindow listenerWindow2 = new MockWindow();
        listener1.setUi(listenerWindow1);
        listener2.setUi(listenerWindow2);
        controller.close();
        Assert.assertEquals(1, listenerWindow1.getCloseCount());
        Assert.assertEquals(1, listenerWindow2.getCloseCount());
    }
}