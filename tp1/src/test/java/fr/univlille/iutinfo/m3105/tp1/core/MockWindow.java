package fr.univlille.iutinfo.m3105.tp1.core;

public class MockWindow implements GenericWindow {
    private int refreshCount = 0;
    private int closeCount = 0;

    @Override
    public void refresh() {
        refreshCount++;
    }

    @Override
    public void close() {
        closeCount++;

    }

    public int getRefreshCount() {
        return refreshCount;
    }
    public int getCloseCount() {
        return closeCount;
    }

}
