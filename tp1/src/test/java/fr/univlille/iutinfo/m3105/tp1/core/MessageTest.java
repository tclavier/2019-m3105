package fr.univlille.iutinfo.m3105.tp1.core;

import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MessageTest {
    @Test
    public  void should_return_all_message_as_single_string(){
        Listener listener = new Listener();
        listener.update(new Message("Premier message"));
        listener.update(new Message("Second message"));
        assertEquals("Premier message\nSecond message", Message.convertListToString(listener.getMessages()));
    }
}
