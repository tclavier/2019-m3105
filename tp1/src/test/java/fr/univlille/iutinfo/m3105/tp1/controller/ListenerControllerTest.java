package fr.univlille.iutinfo.m3105.tp1.controller;

import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;
import org.junit.Test;

import static org.junit.Assert.*;

public class ListenerControllerTest {
    @Test
    public void should_return_all_messages_as_string() {
        Listener listener = new Listener();
        ListenerController controller = new ListenerController(listener);
        listener.update(new Message("Nouveau message"));
        assertEquals("Nouveau message", controller.getMessages());
    }


}