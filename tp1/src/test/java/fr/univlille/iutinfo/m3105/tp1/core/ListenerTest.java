package fr.univlille.iutinfo.m3105.tp1.core;

import fr.univlille.iutinfo.m3105.tp1.domain.Listener;
import fr.univlille.iutinfo.m3105.tp1.domain.Message;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ListenerTest {
    @Test
    public void test_should_refresh_ui_when_update() {
        MockWindow fackWindow = new MockWindow();
        Listener listener = new Listener();
        listener.setUi(fackWindow);
        listener.update(new Message("New message"));
        assertEquals(1, fackWindow.getRefreshCount());
    }
}