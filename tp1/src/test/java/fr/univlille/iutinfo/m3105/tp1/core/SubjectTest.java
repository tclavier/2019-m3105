package fr.univlille.iutinfo.m3105.tp1.core;

import fr.univlille.iutinfo.m3105.tp1.domain.Message;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubjectTest {
    @Test
    public void test_should_notify_one_attached_observer() {
        MockObserver observer = new MockObserver();
        Observable subject = new MockObservable();
        subject.attach(observer);
        subject.notifyObservers(new Message());
        assertEquals(1, observer.getNotifyCount());
    }

    @Test
    public void test_should_not_notify_one_detached_observer() {
        MockObserver observer = new MockObserver();
        Observable subject = new MockObservable();
        subject.attach(observer);
        subject.detach(observer);
        subject.notifyObservers(new Message());
        assertEquals(0, observer.getNotifyCount());
    }
}
